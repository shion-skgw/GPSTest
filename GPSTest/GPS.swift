//
//  GPS.swift
//  GPSTest
//
//  Created by Shion on 2017/03/11.
//  Copyright © 2017 Shion. All rights reserved.
//

import Foundation
import CoreLocation

final class GPS: NSObject, CLLocationManagerDelegate {

	/// CLLocationManager
	private lazy var locationManager: CLLocationManager = {
		let locationManager = CLLocationManager()
		locationManager.delegate = self
		return locationManager
	}()

	/// 最大待ち時間
	/// 指定時間以内にロケーション取得処理が完了しない場合は
	/// 本インスタンスは破棄される
	/// ※面倒なので未実装...
	private let maxWaitingTime: Float

	/// 本インスタンスライフサイクル管理関数
	/// requestLocationにて、本関数でselfを参照させることで
	/// 循環参照を発生させる
	/// また、ロケーション取得処理が完了し次第、本関数にnilが代入されるため
	/// 循環参照が抹消し、本インスタンスが破棄される
	private var lifeCycleController: (([CLLocation]) -> ())?

	/// ロケーション取得処理完了後ハンドラー
	private var handler: (([CLLocation]) -> ())?

	///
	/// コンストラクタ
	/// ロケーションサービスが無効の場合、nilを返却する
	///
	/// - parameter maxWaitingTime	: 最大待ち時間
	///
	init?(maxWaitingTime: Float) {
		if !CLLocationManager.locationServicesEnabled() {
			print("Location Services is Disabled")
			return nil
		}
		print("GPS life cycle start")
		self.maxWaitingTime = maxWaitingTime
	}

	deinit {
		print("GPS life cycle end")
	}

	func requestLocation(didUpdateLocationHandler handler: @escaping ([CLLocation]) -> ()) {
		print("start: \(#function)")
		// 実行後ハンドラー設定
		self.handler = handler

		// 循環参照関数生成
		self.lifeCycleController = {
			(locations: [CLLocation]) -> () in
			// ここで、selfを参照することで
			// lifeCycleController と self が循環参照する
			self.handler!(locations)
		}

		// ロケーション取得処理実行
		self.locationManager.startUpdatingLocation()

		// 意図せずロケーション情報が取得できない場合
		// 本インスタンスが破棄できなくなるため
		// メモリリークの予防措置として、遅延処理により破棄させる
//		DispatchQueue.main.asyncAfter(deadline: .now() + maxWaitingTime) {
//			if self != nil { self.lifeCycleController = nil }
//		}
		print("end: \(#function)")
	}

	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		print("start: \(#function)")
		// ロケーション取得処理終了
		self.locationManager.stopUpdatingLocation()

		// lifeCycleControllerを介して、実行後ハンドラーを実行
		self.lifeCycleController!(locations)

		// self との循環参照を発生させている lifeCycleController を
		// nil 参照にて破棄させる
		// 結果 self の強参照がなくなるため、GPSインスタンスが破棄される
		self.lifeCycleController = nil

		print("end: \(#function)")
	}

}


