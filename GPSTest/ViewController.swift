//
//  ViewController.swift
//  GPSTest
//
//  Created by Shion on 2017/03/11.
//  Copyright © 2017年 Shion. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController {

	@IBOutlet weak var location: UILabel!

	@IBAction func update(_ sender: UIButton) {
		let gps = GPS(maxWaitingTime: 10.0)
		gps?.requestLocation() {
			(locations: [CLLocation]) -> () in
			self.location.text = "Location: \(locations.first)"
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}

}

